Openpush APIs
=============

For this project to be successful we should define precise APIs to be provided on different systems.

# Push Token

A push token (sb got a better name?) allows an application server to send push messages to some client application. The token should be a base64 of a json containing the endpoint for the push message and the token used.
```json
{
    "endpoint": "https://push.example.com/push",
    "token": "THERANDOMPUSHTOKEN"
}
```

In base64 that would be `eyJlbmRwb2ludCI6ICJodHRwczovL3B1c2guZXhhbXBsZS5jb20vcHVzaCIsInRva2VuIjogIlRIRVJBTkRPTVBVU0hUT0tFTiJ9`


In this example the application server could send a push message using a POST to 'https://push.example.com/push/THERANDOMPUSHTOKEN' with message body:
```json
{
  "time_to_live": 15,
  "priority": "high",
  "collapse_key": "Fooooood",
  "data": "Hello world!"
}
```

## Encryption extension

For privacy reasons it would be of interest to implement some encryption in the SDKs. This would keep sensisive message content away from the push server and push service. It would be possible to define some encryption information to be contained in the above push token format so the application server could talk to the client application with e2e encryption. An encryption scheme and shared secret should suffice.

# Server APIs

## Push Server

**considerations**
- The way the server API is designed, allows for the server to drop all its state whenever it likes. Clients will notice that their subscriptions are gone and just resubscribe. (that would drop messages and client applications would need to resync?)
- To restrict who can use the push server we could require authentication on the POST subscription endpoint which effectively restricts all use of the push server.
- There deliberately is no application server authentication. I believe that there is no need for the push server to know what applications a user uses.
- Listening is a separate endpoint with a subpath to allow for different kinds of listening endpoint. plain, sse, http2 push

The push server API is defined using openapi in [openapi.yaml](https://codeberg.org/starpush/starpush-api/src/branch/main/openapi.yaml)

# Client APIs

In general a client API should provide the following interfaces:
- check for availability of the push service
- request and receive a push token
- unsubscribe from push messages
- receive push messages and acknowledge push messages

## Android

The specific design was chosen to allow the push service to be run as a normal user application as well as a system application. Application developers will not have to deal with all of this. That is what SDKs are for. :smirk:

**open questions**
- If the user is able to block apps from using push, should an application be informed? And how? send a null token?
- Is there a reliable and secure way to get the source of a Intent?

**some arguments**
- I prefer receiver to service simply because google appears to be moving away from services and to receivers themselves: see implementation of getIidImplementation in Metadata.java of the firebase.iid
- I am aware that using the below process allows apps to register other apps for the push service. I do not know of an official/endorsed way for getting the sender of an Intent in android. I do not want to employ "hacky" solutions. The most simple mitigation is for the application sdk to remember if it registered for push messages.
- I use PendingIntent for message ack so we can decouple the acking interface from the client application. The push service can freely decide how to handle the ack (service or receiver).

### Check PushService Availability

The client application can check if a push service is available by querying the PackageManager for broadcast receivers for an intent filter with action "net.openpush.android.pushservice.action.REQUEST_TOKEN" and verify that the apps have the permission "net.openpush.android.permission.PUSH_SERVICE". There should be at most one package available that provides both. The client application can check if the push service is a system app to decide if it wants to trust it or if it should ask the user for confirmation.

### Check PushService Availability

The client application can check if a push service is available by querying the PackageManager for broadcast receivers for an intent filter with action "net.openpush.android.pushservice.action.REQUEST_TOKEN" and verify that the apps have the permission "net.openpush.android.permission.PUSH_SERVICE". There should be at most one package available that provides both. The client application can check if the push service is a system app to decide if it wants to trust it.


### Token Request

The client application can request a push token from the android push service. If the application is not registered when the request is processed then a new subscription will be created. Otherwise the push service will answer with the existing push token.

The client application MUST have the permission "net.openpush.android.permission.PUSH_CLIENT".

A token can be requested by sending a broadcast to the push service. The broadcast should request the target to have the "net.openpush.android.permission.PUSH_SERVICE" permission. The intent should adhere to the following constraints:
- intent action: "net.openpush.android.pushservice.action.REQUEST_TOKEN"
- target package: package name of the push service
- extra of type String with key "net.openpush.android.extra.PACKAGE_NAME". The value of this extra is the package name of the application requesting the token.

The push service will answer with a broadcast of its own when the token is available. It requires the requesting application to have the "net.openpush.android.permission.PUSH_CLIENT" permission. The intent guarantees:
- intent action: "net.openpush.android.action.SEND_TOKEN"
- target package: the value of the String extra "net.openpush.android.extra.PACKAGE_NAME" of the request.
- extra of type String with key "net.openpush.android.extra.PUSH_TOKEN". The push token
- extra of type String with key "net.openpush.android.extra.PUSH_ENDPOINT". The endpoint to contact for sending push messages.

The client application declares its receiver with the Intent filter for the "net.openpush.android.action.SEND_TOKEN" action and requires the permission "net.openpush.android.permission.PUSH_SERVICE".

### Unregister from Push Service

The client application can unregister from the push service by sending a broadcast. Intent should fulfill:
- intent action: "net.openpush.android.pushservice.action.UN_REGISTER"
- target package: package name of the push service
- extra of type String with key "net.openpush.android.extra.PUSH_TOKEN". The push token received by the push service

### Push message delivery

When the push service on the android device receives a push message from the server it should send it to the client application. There are valid reasons for the push service to hold back messages to some applications. Especially for conserving battery or for rate limiting.

To deliver a message the push service will send a broadcast to the package (package_name) that is associated with the push subscription to which the message belongs. The broadcast requires the permission "net.openpush.android.permission.PUSH_CLIENT" to be present in the client application. The intent guarantees:
- intent action: "net.openpush.android.action.PUSH_MESSAGE"
- target package: package_name
- extra of type String with key "net.openpush.android.extra.MESSAGE_DATA" that is the message data
- extra of type String with key "net.openpush.android.extra.MESSAGE_ID" that is the unique message ID
- extra of type Parcelable with key "net.openpush.android.extra.MESSAGE_ACK_INTENT" that is the PendingIntent to send for message ack.

The client application sends the pending intent when there is no need to redeliver the message to the application. This is most likely right after message processing.
